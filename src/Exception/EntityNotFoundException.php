<?php
namespace App\Exception;

final class EntityNotFoundException extends \Exception {}
