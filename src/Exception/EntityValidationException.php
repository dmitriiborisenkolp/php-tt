<?php
namespace App\Exception;

final class EntityValidationException extends \Exception {}
