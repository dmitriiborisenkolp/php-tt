<?php
namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Exception\EntityNotFoundException;
use App\Form\Type\BookType;
use App\Service\BooksService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/books")
 */
class BooksController extends AbstractController
{
    /** @var BooksService */
    private $booksServices;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * BooksController constructor.
     * @param BooksService $booksServices
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        BooksService $booksServices,
        EntityManagerInterface $entityManager
    ) {
        $this->booksServices = $booksServices;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="books_index", methods={"GET"})
     */
    public function index(): Response  {
        return $this->render('books/index.html.twig', [
            'books' => array_map(function (Book $book) {
                return [
                    'id' => $book->getId(),
                    'cover' => $book->getCover(),
                    'form' => $this->createForm(BookType::class, $book)->createView(),
                ];
            }, $this->booksServices->getBooks()),
        ]);
    }

    /**
     * @Route("/add", name="books_add", methods={"GET", "POST"})
     */
    public function add(Request $request): Response {
        $form = $this->createForm(BookType::class, new Book());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->beginTransaction();

            try {
                /** @var UploadedFile $cover */
                /** @var Book $book */
                $book = $form->getData();
                $cover = $form['cover']->getData();

                if ($cover) {
                    $originalFilename = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);

                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$cover->guessExtension();

                    $cover->move($this->getParameter('book_covers_directory'), $newFilename);

                    $book->setCover($newFilename);
                }

                $this->booksServices->addBook($book);
                $this->entityManager->commit();

                return $this->redirectToRoute('books_index');
            } catch (\Exception $e) {
                $this->entityManager->rollback();

                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('books/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="books_edit", methods={"GET", "POST"}, requirements={"id"="\d+"})
     */
    public function edit(Request $request): Response {
        try {
            $book = $this->booksServices->getById($request->get('id'));

            $form = $this->createForm(BookType::class, $book);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->beginTransaction();

                try {
                    /** @var UploadedFile $cover */
                    /** @var Book $book */
                    $book = $form->getData();
                    $cover = $form['cover']->getData();

                    if ($cover) {
                        $originalFilename = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);

                        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                        $newFilename = $safeFilename.'-'.uniqid().'.'.$cover->guessExtension();

                        $cover->move($this->getParameter('book_covers_directory'), $newFilename);

                        $book->setCover($newFilename);
                    }

                    $this->booksServices->saveBook($book);
                    $this->entityManager->commit();

                    return $this->redirectToRoute('books_index');
                } catch (\Exception $e) {
                    $this->entityManager->rollback();

                    $form->addError(new FormError($e->getMessage()));
                }
            }
        } catch (EntityNotFoundException $e) {
            throw $this->createNotFoundException();
        }

        return $this->render('books/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="books_delete", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function delete(Request $request): Response {
        $bookId = $request->get('id');

        $this->entityManager->beginTransaction();

        try {
            $this->booksServices->deleteBook($bookId);
            $this->entityManager->commit();

            return $this->redirectToRoute('books_index');
        } catch (\Exception $e) {
            $this->entityManager->rollback();

            throw $e;
        }
    }

    /**
     * @Route("/patch/{id}", name="books_patch", methods={"PATCH"}, requirements={"id"="\d+"})
     */
    public function patch(Request $request): Response {
        $bookId = $request->get('id');
        $jsonData = json_decode($request->getContent(), true);

        $this->entityManager->beginTransaction();

        try {
            $this->booksServices->patchBook($bookId, $jsonData['field'], $jsonData['value']);
            $this->entityManager->commit();

            return $this->json([
                'success' => true,
            ]);
        } catch (\Exception $e) {
            $this->entityManager->rollback();

            throw $e;
        }
    }

    /**
     * @Route("/getBooksCreateByAtLeast2Authors", methods={"GET"}, name="books_get_books_create_by_at_least_2_authors")
     */
    public function getBooksCreateByAtLeast2Authors(): Response {
        return $this->json([
            'books' => $this->booksServices->getBooksCreateByAtLeast2Authors(),
        ]);
    }
}
