<?php
namespace App\Controller;

use App\Entity\Author;
use App\Exception\EntityNotFoundException;
use App\Form\Type\AuthorType;
use App\Service\AuthorsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/authors")
 */
class AuthorsController extends AbstractController
{
    /** @var AuthorsService */
    private $authorsServices;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * AuthorsController constructor.
     * @param AuthorsService $authorsServices
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        AuthorsService $authorsServices,
        EntityManagerInterface $entityManager
    ) {
        $this->authorsServices = $authorsServices;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="authors_index", methods={"GET"})
     */
    public function index(): Response  {
        return $this->render('authors/index.html.twig', [
            'authors' => array_map(function (Author $author) {
                return [
                    'id' => $author->getId(),
                    'name' => $author->getName(),
                ];
            }, $this->authorsServices->getAuthors()),
        ]);
    }

    /**
     * @Route("/add", name="authors_add", methods={"GET", "POST"})
     */
    public function add(Request $request): Response {
        $form = $this->createForm(AuthorType::class, new Author());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->beginTransaction();

            try {
                $author = $form->getData();
                $this->authorsServices->addAuthor($author);
                $this->entityManager->commit();

                return $this->redirectToRoute('authors_index');
            } catch (\Exception $e) {
                $this->entityManager->rollback();

                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('authors/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="authors_edit", methods={"GET", "POST"}, requirements={"id"="\d+"}  )
     */
    public function edit(Request $request): Response {
        try {
            $author = $this->authorsServices->getById($request->get('id'));

            $form = $this->createForm(AuthorType::class, $author);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->beginTransaction();

                try {
                    $this->authorsServices->saveAuthor($form->getData());
                    $this->entityManager->commit();

                    return $this->redirectToRoute('authors_index');
                } catch (\Exception $e) {
                    $this->entityManager->rollback();

                    $form->addError(new FormError($e->getMessage()));
                }
            }
        } catch (EntityNotFoundException $e) {
            throw $this->createNotFoundException();
        }

        return $this->render('authors/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="authors_delete", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function delete(Request $request): Response {
        $authorId = $request->get('id');

        $this->entityManager->beginTransaction();

        try {
            $this->authorsServices->deleteAuthor($authorId);
            $this->entityManager->commit();

            return $this->redirectToRoute('authors_index');
        } catch (\Exception $e) {
            $this->entityManager->rollback();

            throw $e;
        }
    }
}
