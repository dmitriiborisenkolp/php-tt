<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function createAuthor(Author $author): void  {
        $this->getEntityManager()->persist($author);
        $this->getEntityManager()->flush($author);
    }

    public function updateAuthor(Author $author): void  {
        $this->getEntityManager()->persist($author);
        $this->getEntityManager()->flush($author);
    }

    public function deleteAuthorById(int $id): void {
        $author = $this->find($id);

        $this->getEntityManager()->remove($author);
        $this->getEntityManager()->flush($author);
    }
}
