<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function createBook(Book $book): void  {
        $this->getEntityManager()->persist($book);
        $this->getEntityManager()->flush($book);
    }

    public function updateBook(Book $book): void  {
        $this->getEntityManager()->persist($book);
        $this->getEntityManager()->flush($book);
    }

    public function deleteBookById(int $id): void {
        $book = $this->find($id);

        $this->getEntityManager()->remove($book);
        $this->getEntityManager()->flush($book);
    }
}
