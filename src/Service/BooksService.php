<?php
namespace App\Service;

use App\Entity\Book;
use App\Exception\EntityNotFoundException;
use App\Exception\EntityValidationException;
use App\Repository\BookRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class BooksService
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var BookRepository */
    private $bookRepository;

    /** @var AuthorsService */
    private $authorService;

    /**
     * BooksService constructor.
     * @param ValidatorInterface $validator
     * @param BookRepository $bookRepository
     * @param AuthorsService $authorService
     */
    public function __construct(
        ValidatorInterface $validator,
        BookRepository $bookRepository,
        AuthorsService $authorService
    ) {
        $this->validator = $validator;
        $this->bookRepository = $bookRepository;
        $this->authorService = $authorService;
    }

    /**
     * Returns book by Id
     * @param int $id
     * @return Book
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Book {
        $result = $this->bookRepository->find($id);

        if ($result === null) {
            throw new EntityNotFoundException(sprintf('Book with Id "%d" was not found', $id));
        } else {
            return $result;
        }
    }

    /**
     * Returns list of books
     * @return Book[]
     */
    public function getBooks(): array {
        return $this->bookRepository->findAll();
    }

    /**
     * Add book
     * @param Book $book
     * @return Book
     * @throws EntityValidationException
     */
    public function addBook(Book $book): Book {
        $errors = $this->validator->validate($book);

        if (count($errors) > 0) {
            throw new EntityValidationException(join("\n", $errors));
        }

        $this->bookRepository->createBook($book);

        return $book;
    }

    /**
     * Update book
     * @param Book $book
     * @throws EntityValidationException
     */
    public function saveBook(Book $book): void  {
        $errors = $this->validator->validate($book);

        if (count($errors) > 0) {
            throw new EntityValidationException(join("\n", $errors));
        }

        $this->bookRepository->updateBook($book);
    }

    /**
     * Delete book by Id
     * @param int $id
     */
    public function deleteBook(int $id): void  {
        $this->bookRepository->deleteBookById($id);
    }

    /**
     * Patch book
     * @param int $id
     * @param string $field
     * @param $value
     * @throws EntityNotFoundException
     */
    public function patchBook(int $id, string $field, $value): void {
        $book = $this->getById($id);

        switch ($field) {
            default:
                throw new \Exception(sprintf('Unknown field "%s"', $field));

            case 'title':
                $book->setTitle($value);
                break;

            case 'description':
                $book->setDescription($value);
                break;

            case 'authors':
                $book->setAuthors(
                    $this->authorService->getAuthorByIds($value)
                );
                break;

            case 'published_at':
                $book->setPublishedAt(new \DateTime(sprintf('@%d', $value)));
                break;
        }

        $this->bookRepository->updateBook($book);
    }

    /**
     * @return array
     */
    public function getBooksCreateByAtLeast2Authors(): array {
        /**
            select b.*, count(ba.*) as num_authors
            from book b
            left join book_author as ba ON ba.book_id = b.id
            group by b.id
            having count(ba.*) >= 2
         */

        return $this->bookRepository->createQueryBuilder('b')
            ->select(['b', 'count(ba) as num_authors'])
            ->leftJoin('b.authors', 'ba')
            ->groupBy('b.id')
            ->having('count(ba) >= 2')
            ->getQuery()
            ->execute();
    }
}
