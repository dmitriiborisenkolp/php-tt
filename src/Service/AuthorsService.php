<?php
namespace App\Service;

use App\Entity\Author;
use App\Exception\EntityNotFoundException;
use App\Exception\EntityValidationException;
use App\Repository\AuthorRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class AuthorsService
{
    /** @var ValidatorInterface */
    private $validator;

    /** @var AuthorRepository */
    private $authorsRepository;

    /**
     * AuthorsService constructor.
     * @param ValidatorInterface $validator
     * @param AuthorRepository $authorsRepository
     */
    public function __construct(
        ValidatorInterface $validator,
        AuthorRepository $authorsRepository
    ) {
        $this->validator = $validator;
        $this->authorsRepository = $authorsRepository;
    }

    /**
     * Returns list of authors
     * @return Author[]
     */
    public function getAuthors(): array {
        return $this->authorsRepository->findAll();
    }

    /**
     * Returns authors by Ids
     * @param array $ids
     * @return array
     */
    public function getAuthorByIds(array $ids): array {
        return $this->authorsRepository->findBy([
            'id' => $ids,
        ]);
    }

    /**
     * @param Author $author
     * @return Author
     * @throws EntityValidationException
     */
    public function addAuthor(Author $author): Author {
        $errors = $this->validator->validate($author);

        if (count($errors) > 0) {
            throw new EntityValidationException(join("\n", $errors));
        }

        $this->authorsRepository->createAuthor($author);

        return $author;
    }

    /**
     * Update author
     * @param Author $author
     * @throws EntityValidationException
     */
    public function saveAuthor(Author $author): void  {
        $errors = $this->validator->validate($author);

        if (count($errors) > 0) {
            throw new EntityValidationException(join("\n", $errors));
        }

        $this->authorsRepository->updateAuthor($author);
    }

    /**
     * Delete author by Id
     * @param int $id
     */
    public function deleteAuthor(int $id): void  {
        $this->authorsRepository->deleteAuthorById($id);
    }

    /**
     * Returns author by Id
     * @param int $id
     * @return Author
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Author {
        $result = $this->authorsRepository->find($id);

        if ($result === null) {
            throw new EntityNotFoundException(sprintf('Author with Id "%d" was not found', $id));
        } else {
            return $result;
        }
    }
}
