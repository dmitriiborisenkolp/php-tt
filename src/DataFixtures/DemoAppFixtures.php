<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DemoAppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (['Foo', 'Bar', 'Baz'] as $authorName) {
            $author = new Author();
            $author->setName($authorName);

            foreach (['Book 1', 'Book 2', 'Book 3'] as $bookName) {
                $book = new Book();
                $book->setTitle($bookName);
                $book->setDescription(sprintf('Description for book "%s"', $bookName));
                $book->setPublishedAt(new \DateTime());
                $book->setCover('demo_cover.png');
                $book->addAuthor($author);

                $manager->persist($book);
            }

            $manager->persist($author);
        }

        $manager->flush();
    }
}
